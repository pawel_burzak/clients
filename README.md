# Clients RESTful Api #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

#### This application manages, stores people and contacts in database. It provides Person CRUD and Contact CRUD operations. Authorization and authentication are ignored.####

### How do I get set up? ###

#### For running the project you need: ####
* JDK 1.8 or later
* Maven 3 or later
* MySQL Server 5.7 or later and MySQL Workbench

#### Dependencies used in the project: ####
* Spring Boot
* Model Mapper
* MySQL Connector
    
#### Database setup: ####
* Create new User with all privileges:  
	Login Name: root  
	Password: root  
* Create new Schema:  
	Name: britenettask  

#### Steps how to build and run the project: #####
* Connect to MySQL database in your IDE, for example instruction for IntelliJ Idea 2017.3: https://www.jetbrains.com/help/idea/connecting-to-a-database.html#mysql
* Run script in terminal in your IDE:  
	mvn spring-boot:run

### Example usages: ###  
Person contains fields:  
* id – Auto-generated number, forbidden  
* version – Auto-generated version number, forbidden  
* firstName – Person first name, required  
* lastName – Person last name, required  
* sex – Person sex, required  
* contacts – List of Person contacts, not required  
  
Contact contains fields:  
* id – Auto-generated number, forbidden  
* version – Auto-generated version number, forbidden  
* phoneNumber – Contact phone number , not required  
* email – Contact email, not required  
* address – Contact address, not required  
   
#### List of People   
* URL: /britenettask/person/  
* Method: GET  
* URL Params: None  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
        [{  
          "id": 1,  
          "version": 0,  
          "firstName": "Jan",  
          "lastName": "Kowalski",  
          "sex": "MALE",  
          "contacts":  [{  
				"id": 1,  
          		"version": 0,  
         		 "phoneNumber": "123456789",  
         		 "email": "ak@gmail.com",  
		 		"address": "Warsaw"  
			}]  
		}, {  
          "id": 2,  
          "version": 0,  
          "firstName": "Anna",  
          "lastName": "Nowak",  
          "sex": "FEMALE,  
          "contacts":  [{  
				"id": 2,  
          		"version": 0,  
         		 "phoneNumber": "987654321",  
         		 "email": "an@gmail.com",  
		 		"address": "Lublin"  
			}]  
        }]  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/person/  
      
#### List of Contacts  
* URL: /britenettask/contact/  
* Method: GET  
* URL Params: None  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
        [{  
			"id": 1,  
			"version": 0,  
			"phoneNumber": "123456789",  
			"email": "ak@gmail.com",   
			"address": "Warsaw"  
			},{  
			"id": 2,  
			"version": 0,  
			"phoneNumber": "987654321",  
			"email": "an@gmail.com",   
			"address": "Lublin"   
		}]  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/contact/  

#### Show Person
* URL: /britenettask/person/:id  
* Method: GET  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
		{       
          "id": 1,  
          "version": 0,  
          "firstName": "Jan",  
          "lastName": "Kowalski",  
          "sex": "MALE",  
          "contacts":  [{  
				"id": 1,  
          		"version": 0,  
         		 "phoneNumber": "123456789",  
         		 "email": "ak@gmail.com",  
		 		"address": "Warsaw"  
			}]  
		}  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/person/1  

#### Show Contact
* URL: /britenettask/contact/:id  
* Method: GET  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
			{       
 				"id": 1,  
          		"version": 0,  
         		 "phoneNumber": "123456789",  
         		 "email": "ak@gmail.com",  
				 "address": "Warsaw"  
			}  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/contact/1  

#### Create Person
* URL: /britenettask/person    
* Method: POST  
* URL Params: None  
+ Data Params:  
     * JSON (application/json)  
		{      
          "firstName": "Jan",  
          "lastName": "Kowalski",  
          "sex": "MALE",  
          "contacts":  
          [{  
         		 "phoneNumber": "123456789",  
         		 "email": "ak@gmail.com",  
		 		"address": "Warsaw"  
			}]  
		}  
+ Success Response:  
     * Code: 201 CREATED,  
+ Error response:  
     * Code: 400 BAD REQUEST  
+ Sample call:  
     * http://localhost:8080/britenettask/person    
     * JSON (application/json)  
		{      
          "firstName": "Jan",  
          "lastName": "Kowalski",  
          "sex": "MALE",  
          "contacts":  
          [{  
         		 "phoneNumber": "123456789",  
         		 "email": "ak@gmail.com",  
		 		"address": "Warsaw"  
			}]  
		}  

#### Create Contact
* URL: /britenettask/person/:id/contact/    
* Method: POST  
* URL Params: id=[long]  
+ Data Params:  
     * JSON (application/json)  
		{      
			"phoneNumber": "123456789",  
         	"email": "ak@gmail.com",  
		 	"address": "Warsaw"  
		}  
+ Success Response:  
     * Code: 201 CREATED,  
+ Error response:  
     * Code: 404 NOT FOUND  
	OR  
     * Code: 400 BAD REQUEST  
+ Sample call:  
     * http://localhost:8080/ britenettask/person/1/contact/    
     * JSON (application/json)  
		{      
         	"phoneNumber": "123456789",  
         	"email": "ak@gmail.com",  
		 	"address": "Warsaw"  
		}  

#### Update Person  
* URL: /britenettask/person/:id  
* Method: PUT  
* URL Params: id=[long]  
+ Data Params:  
     * JSON (application/json)  
		{      
          "firstName": "Jan",  
          "lastName": "Adamski",  
          "sex": "MALE"  
		}  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 400 BAD REQUEST  
      OR  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/person/1  
     * JSON (application/json)  
      	{      
          "firstName": "Jan",  
          "lastName": "Adamski",  
          "sex": "MALE"  
		}  
 
#### Update Contact  
* URL: /britenettask/contact/:id  
* Method: PUT  
* URL Params: id=[long]  
+ Data Params:  
     * JSON (application/json)  
		{      
         	"phoneNumber": "987654321",  
         	"email": "ak@gmail.com",  
		 	"address": "Warsaw"   
		}  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 400 BAD REQUEST  
      OR  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettask/contact/1  
     * JSON (application/json)  
		{      
         	"phoneNumber": "987654321",  
         	"email": "ak@gmail.com",  
		 	"address": "Warsaw"  
		}  

     
#### Delete Person
* URL: /britenettast/person/:id  
* Method: DELETE  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettast/person/1  

#### Delete Contact
* URL: /britenettast/contact/:id  
* Method: DELETE  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/britenettast/contact/1  
