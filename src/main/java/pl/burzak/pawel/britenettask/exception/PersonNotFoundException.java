package pl.burzak.pawel.britenettask.exception;

public class PersonNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 7395159788936614923L;
}
