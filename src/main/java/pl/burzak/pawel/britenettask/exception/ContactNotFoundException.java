package pl.burzak.pawel.britenettask.exception;

public class ContactNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -4751119442196908910L;
}
