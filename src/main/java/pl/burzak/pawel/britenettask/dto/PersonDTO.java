package pl.burzak.pawel.britenettask.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.validator.constraints.NotBlank;
import pl.burzak.pawel.britenettask.util.enumValidator.Sex;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class PersonDTO implements Serializable {
    private static final long serialVersionUID = 8919761830514742928L;

    private Long id;

    private Long version;

    @NotBlank(message = "Invalid first name, field can't be empty.")
    private String firstName;

    @NotBlank(message = "Invalid last name, field can't be empty.")
    private String lastName;

    @NotBlank(message = "Invalid sex, field can't be empty.")
    @Sex(enumClass = pl.burzak.pawel.britenettask.domain.util.Sex.class, ignoreCase = true,
            message = "Invalid sex, choose between 'Male' and 'Female'.")
    private String sex;

    @JsonManagedReference
    private Set<ContactDTO> contacts = new HashSet<>();

    public PersonDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Set<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(Set<ContactDTO> contacts) {
        this.contacts = contacts;
    }
}
