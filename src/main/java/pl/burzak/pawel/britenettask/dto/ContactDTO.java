package pl.burzak.pawel.britenettask.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

public class ContactDTO implements Serializable {
    private static final long serialVersionUID = 2451382276482916441L;

    private Long id;

    private Long version;

    @Pattern(regexp = "(^$|[0-9]{9})", message = "Invalid phone number, use only 9 digits without spaces.")
    private String phoneNumber;

    @Email(message = "Invalid email, use pattern 'example@email.com'.")
    private String email;

    private String address;

    @JsonBackReference
    private PersonDTO person;

    public ContactDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }
}
