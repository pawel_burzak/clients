package pl.burzak.pawel.britenettask.util;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.Errors;

import java.util.stream.Collectors;

public class ResponseBody {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(Errors errors) {
        this.message = errors.getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(" "));
    }
}
