package pl.burzak.pawel.britenettask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.burzak.pawel.britenettask.domain.entity.Person;
import pl.burzak.pawel.britenettask.exception.PersonNotFoundException;
import pl.burzak.pawel.britenettask.repository.PersonRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Long create(Person person) {
        personRepository.save(person);

        return person.getId();
    }

    public List<Person> findAllWithContacts() {
        return personRepository.findAllWithContacts();
    }

    public Person findByIdWithContacts(Long id) {
        Optional<Person> person = personRepository.findByIdWithContacts(id);

        if (!person.isPresent()) {
            throw new PersonNotFoundException();
        }

        return person.get();
    }

    public Person findById(Long id) {
        Optional<Person> person = personRepository.findById(id);

        if (!person.isPresent()) {
            throw new PersonNotFoundException();
        }

        return person.get();
    }

    public void update(Person person) {
        Optional<Person> optionalPerson = personRepository.findById(person.getId());

        if (!optionalPerson.isPresent()) {
            throw new PersonNotFoundException();
        }

        optionalPerson.get().setFirstName(person.getFirstName());
        optionalPerson.get().setLastName(person.getLastName());
        optionalPerson.get().setSex(person.getSex());
    }

    public void delete(Long id) {
        Optional<Person> optionalPerson = personRepository.findById(id);

        if (!optionalPerson.isPresent()) {
            throw new PersonNotFoundException();
        }

        personRepository.delete(id);
    }
}
