package pl.burzak.pawel.britenettask.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.burzak.pawel.britenettask.domain.entity.Contact;
import pl.burzak.pawel.britenettask.exception.ContactNotFoundException;
import pl.burzak.pawel.britenettask.repository.ContactRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ContactService {

    private final ContactRepository contactRepository;

    @Autowired
    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public Long create(Contact contact) {
        contactRepository.save(contact);

        return contact.getId();
    }

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public Contact findById(Long id) {
        Optional<Contact> contact = contactRepository.findById(id);

        if (!contact.isPresent()) {
            throw new ContactNotFoundException();
        }

        return contact.get();
    }

    public void update(Contact contact) {
        Optional<Contact> optionalContact = contactRepository.findById(contact.getId());

        if (!optionalContact.isPresent()) {
            throw new ContactNotFoundException();
        }

        optionalContact.get().setAddress(contact.getAddress());
        optionalContact.get().setEmail(contact.getEmail());
        optionalContact.get().setPhoneNumber(contact.getPhoneNumber());
    }

    public void delete(Long id) {
        Optional<Contact> optionalContact = contactRepository.findById(id);

        if (!optionalContact.isPresent()) {
            throw new ContactNotFoundException();
        }

        contactRepository.delete(id);
    }
}
