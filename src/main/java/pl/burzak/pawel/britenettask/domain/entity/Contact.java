package pl.burzak.pawel.britenettask.domain.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CONTACT")
@AttributeOverride(name = "ID", column = @Column(name = "CONTACT_ID"))
public class Contact extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 7958402400479388753L;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "ADDRESS")
    private String address;

    @ManyToOne
    @JoinColumn(name = "PERSON_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_CONTACT_PERSON"))
    private Person person;

    public Contact() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
