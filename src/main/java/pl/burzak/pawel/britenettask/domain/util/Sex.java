package pl.burzak.pawel.britenettask.domain.util;

public enum Sex {
    MALE,
    FEMALE
}
