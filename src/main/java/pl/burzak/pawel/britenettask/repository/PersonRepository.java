package pl.burzak.pawel.britenettask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.burzak.pawel.britenettask.domain.entity.Person;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value = "SELECT p FROM Person p LEFT JOIN FETCH p.contacts WHERE p.id = :id")
    Optional<Person> findById(@Param("id") Long id);

    @Query(value = "SELECT distinct p FROM Person p LEFT JOIN FETCH p.contacts")
    List<Person> findAllWithContacts();

    @Query(value = "SELECT p FROM Person p LEFT JOIN FETCH p.contacts WHERE p.id = :id")
    Optional<Person> findByIdWithContacts(@Param("id") Long id);
}
