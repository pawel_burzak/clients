package pl.burzak.pawel.britenettask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.burzak.pawel.britenettask.domain.entity.Contact;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query(value = "SELECT c FROM Contact c LEFT JOIN FETCH c.person WHERE c.id = :id")
    Optional<Contact> findById(@Param("id") Long id);

    @Override
    @Query(value = "SELECT distinct c FROM Contact c LEFT JOIN fetch c.person")
    List<Contact> findAll();
}
