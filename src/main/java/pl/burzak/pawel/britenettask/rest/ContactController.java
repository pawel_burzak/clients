package pl.burzak.pawel.britenettask.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.burzak.pawel.britenettask.domain.entity.Contact;
import pl.burzak.pawel.britenettask.domain.entity.Person;
import pl.burzak.pawel.britenettask.dto.ContactDTO;
import pl.burzak.pawel.britenettask.exception.ContactNotFoundException;
import pl.burzak.pawel.britenettask.exception.PersonNotFoundException;
import pl.burzak.pawel.britenettask.service.ContactService;
import pl.burzak.pawel.britenettask.service.PersonService;
import pl.burzak.pawel.britenettask.util.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ContactController {

    private final ContactService contactService;
    private final PersonService personService;
    private final ModelMapper modelMapper;

    @Autowired
    public ContactController(ContactService contactService, PersonService personService, ModelMapper modelMapper) {
        this.contactService = contactService;
        this.personService = personService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/person/{id}/contact/")
    public ResponseEntity<ResponseBody> create(@PathVariable("id") Long id, @Valid @RequestBody ContactDTO contactDTO, Errors errors) {
        if (errors.hasErrors()) {
            ResponseBody result = new ResponseBody();
            result.setMessage(errors);

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            Person person = personService.findById(id);
            Contact contact = convertToEntity(contactDTO);
            contact.setPerson(person);
            contactService.create(contact);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (PersonNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/contact/")
    public ResponseEntity<List<ContactDTO>> getAll() {
        List<ContactDTO> allContactsDTO = contactService.findAll().stream()
                .map(contact -> convertToDTO(contact))
                .collect(Collectors.toList());

        return new ResponseEntity<>(allContactsDTO, !allContactsDTO.isEmpty() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/contact/{id}")
    public ResponseEntity<ContactDTO> get(@PathVariable("id") Long id) {
        try {
            ContactDTO contactDTO = convertToDTO(contactService.findById(id));
            return new ResponseEntity<>(contactDTO, HttpStatus.OK);
        } catch (ContactNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/contact/{id}")
    public ResponseEntity<ResponseBody> update(@PathVariable("id") Long id, @Valid @RequestBody ContactDTO contactDTO, Errors errors) {
        if (errors.hasErrors()) {
            ResponseBody result = new ResponseBody();
            result.setMessage(errors);

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            Contact contact = convertToEntity(contactDTO);
            contact.setId(id);
            contactService.update(contact);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ContactNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/contact/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        try {
            contactService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ContactNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private Contact convertToEntity(ContactDTO contactDTO) {
        Contact contact = modelMapper.map(contactDTO, Contact.class);
        return contact;
    }

    private ContactDTO convertToDTO(Contact contact) {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setId(contact.getId());
        contactDTO.setVersion(contact.getVersion());
        contactDTO.setPhoneNumber(contact.getPhoneNumber());
        contactDTO.setEmail(contact.getEmail());
        contactDTO.setAddress(contact.getAddress());
        return contactDTO;
    }
}
