package pl.burzak.pawel.britenettask.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.burzak.pawel.britenettask.domain.entity.Person;
import pl.burzak.pawel.britenettask.dto.PersonDTO;
import pl.burzak.pawel.britenettask.exception.PersonNotFoundException;
import pl.burzak.pawel.britenettask.service.PersonService;
import pl.burzak.pawel.britenettask.util.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PersonController {

    private final PersonService personService;
    private final ModelMapper modelMapper;

    @Autowired
    public PersonController(PersonService personService, ModelMapper modelMapper) {
        this.personService = personService;
        this.modelMapper = modelMapper;
    }

    @PostMapping("/person/")
    public ResponseEntity<ResponseBody> create(@Valid @RequestBody PersonDTO personDTO, Errors errors) {
        if (errors.hasErrors()) {
            ResponseBody result = new ResponseBody();
            result.setMessage(errors);

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        personService.create(convertToEntity(personDTO));

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/person/")
    public ResponseEntity<List<PersonDTO>> getAll() {
        List<PersonDTO> allPeopleDTO = personService.findAllWithContacts().stream()
                .map(person -> convertToDTO(person))
                .collect(Collectors.toList());

        return new ResponseEntity<>(allPeopleDTO, !allPeopleDTO.isEmpty() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/person/{id}")
    public ResponseEntity<PersonDTO> get(@PathVariable("id") Long id) {
        try {
            PersonDTO personDTO = convertToDTO(personService.findByIdWithContacts(id));
            return new ResponseEntity<>(personDTO, HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/person/{id}")
    public ResponseEntity<ResponseBody> update(@PathVariable("id") Long id, @Valid @RequestBody PersonDTO personDTO, Errors errors) {
        if (errors.hasErrors()) {
            ResponseBody result = new ResponseBody();
            result.setMessage(errors);

            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }

        try {
            Person person = convertToEntity(personDTO);
            person.setId(id);
            personService.update(person);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/person/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        try {
            personService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (PersonNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private Person convertToEntity(PersonDTO personDTO) {
        Person person = modelMapper.map(personDTO, Person.class);
        return person;
    }

    private PersonDTO convertToDTO(Person person) {
        PersonDTO personDTO = modelMapper.map(person, PersonDTO.class);
        return personDTO;
    }
}
